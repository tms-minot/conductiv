import json
import csv
import torch
from torch.autograd import Variable
from torch.autograd import Function
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch import optim
from vae import VAE, lat,in_c

from random import randint,shuffle

from os import listdir
from os.path import isfile, join

from scipy import signal
import numpy as np

path = 'test_assignment_sim.csv'

### DATA LOADER
with open(path,'rt') as f:
    tab = csv.reader(f)
    next(tab)
    data = []
    X = []
    Y = []
    for  line in tab:
        data.append([float(x) for x in line])

shuffle(data)
for D in data:
    X.append(np.array([x for x in D[:4]]))
    Y.append(np.array([y for y in D[4:]]))

#X = np.stack(X)
#Y = np.stack(Y)
split = int(0.8*len(X))

net = VAE().cuda()
#net.load_state_dict(torch.load("../model/VAE_SMAP2_ep1_0_ep2_3500.pth"))
net.get_shapes((10,in_c))
net.train()
optimizer = optim.Adam(net.parameters(), lr=1e-3)
batch_size = 32
ep1 = 800
ep2 = 800

stage=1

iter = split// batch_size
iter_t = (len(X)-split)//batch_size
for epoch in range(ep1+ep2):
    if epoch >= ep1:
        stage = 2

    for i in range(iter):
        inputs = Variable( torch.FloatTensor(X[i*batch_size:(i+1)*batch_size])).cuda()
        outputs = Variable( torch.FloatTensor(Y[i*batch_size:(i+1)*batch_size])).cuda()
        optimizer.zero_grad()
        x, sigma2,z, mu, logvar = net(inputs,stage)

        loglike = (x - outputs).pow(2) / sigma2
        KLD = - torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
        #KLD =  - 0.5 * torch.sum(1 + var2.log() - mu.pow(2) - var2)
        like = torch.sum(loglike)
        loss = (like + KLD + torch.sum(torch.log(torch.sqrt(sigma2/2))))/2
        loss.backward()
        optimizer.step()

        T = loss.detach().cpu().numpy()
        KL = KLD.detach().cpu().numpy()
        REC = like.detach().cpu().numpy()

        print( f'VAE ===> Epoch: {epoch} T: {T:.7f} | KL: {KL:.7f} | REC: {REC:.7f}' )

torch.save(net.state_dict(), f'VAE_ep1_{ep1//1000}_ep2_{ep2//1000}.pth')


net.eval()


inputs = Variable( torch.FloatTensor(X[split:])).cuda()
outputs = Variable( torch.FloatTensor(Y[split:])).cuda()

x, sigma2,z, mu, logvar = net(inputs,stage)

loglike = (x - outputs).pow(2) / sigma2
KLD = - torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
#KLD =  - 0.5 * torch.sum(1 + var2.log() - mu.pow(2) - var2)
like = torch.sum(loglike)
loss = (like + KLD + torch.sum(torch.log(torch.sqrt(sigma2/2))))/2

err  = ((x - outputs)/x).abs().mean().detach().cpu().numpy()
print (f'Mean relative error: {float(err)}')
T = loss.detach().cpu().numpy()
KL = KLD.detach().cpu().numpy()
REC = like.detach().cpu().numpy()
