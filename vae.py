import torch
from torch.autograd import Variable
from torch.autograd import Function
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter


out_c = 49
lat =100
in_c = 4

enc_params = {
        "filter": [    256,       128,        lat*2],
        "kernel": [    129,       129,      65,       ],
        "padding":[     64,        64,      32,       ],
        "stride": [      1,         2,       1,        ]
}
dec_params = {
        "filter": [ lat,       512,     1024     ],
        "kernel": [     32,        34,      66 ],
        "padding":[      0,        16,      32],
        "stride": [      1,         2,       2]
}

class Enc(nn.Module):
    def __init__(self,in_chans,enc_params):
        super(Enc, self).__init__()
        x = in_chans
        layers = []
        for i,y in  enumerate(enc_params["filter"]):
            layers.append(nn.Linear(x, y))
            if i != len(enc_params["filter"])-1:
                layers.append(nn.BatchNorm1d(y))
                layers.append(nn.LeakyReLU())
            x = y

        self.mod = nn.Sequential(*layers)
    def forward(self, x):
        y = self.mod(x)
        return y

class Dec(nn.Module):
    def __init__(self,out_chans,dec_params):
        super(Dec, self).__init__()
        layers = []
        for i,x in  enumerate(dec_params["filter"]):

            if i != len(dec_params["filter"])-1:
                y = dec_params["filter"][i+1]
                layers.append(nn.Linear(x, y))
                layers.append(nn.BatchNorm1d(y))
                layers.append(nn.LeakyReLU())

        layers.append(nn.Linear(x, out_chans))
        self.mod = nn.Sequential(*layers)

    def forward(self, x):
        y = self.mod(x)
        return y



class VAE(nn.Module):
    def __init__(self):
        super(VAE, self).__init__()

        self.enc = Enc(in_c,enc_params)
        self.recdec = Dec(out_c,dec_params)
        self.vardec = Dec(out_c,dec_params)

        self.sigma = Parameter(torch.ones(1))
        self.latent = lat
    def forward(self,x,stage):
        x = self.enc(x)
        z, mu, logvar = self.reparam(x)
        x = self.recdec(z)
        if stage == 2:
            self.sigma.requires_grad=False
            sigma2 =self.sigma.unsqueeze(1).pow(2) +2* self.vardec(z).pow(2) # nn.functional.softplus(self.sigma2.unsqueeze(1)/2) +nn.functional.softplus(self.vardec(z))
        else:
            sigma2 = 2 * self.sigma.pow(2).unsqueeze(1) #nn.functional.softplus(self.sigma2.unsqueeze(1))
        return x, sigma2, z, mu, logvar

    def reparam(self,x):
        mu, logvar = x.split(self.latent, dim=1)
        eps = torch.randn_like(mu)
        std = torch.exp(0.5*logvar)
        z = eps * std + mu
        return z, mu, logvar

    def get_shapes(self, xshape):

        X = torch.zeros(xshape).cuda()
        print("Encoder shapes:")
        for e in self.enc.mod:
            X = e(X)
            print(X.shape)

        X,_ = X.split(self.latent, dim=1)
        print("Decoder shapes:")
        print(X.shape)
        for e in self.recdec.mod:
            X = e(X)
            print(X.shape)
